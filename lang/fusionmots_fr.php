<?php
/** Fichier de langue de SPIP **/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	'erreur_par'		=>'Choisissez un mot de remplacement.',
	'erreur_remplacer'	=>'Choisissez au moins un mot à remplacer.',
	'explication'		=>'Sur cette page vous pouvez fusionner des mots-clefs. Choisissez, dans la colonne de gauche du tableau, les mots-clefs qui seront remplacés au profit de celui choisi dans la colonne de droite.',
	'aucun_groupe'	=>'Aucun groupe de mots n\'est sélectionné.',
	'choisir_groupe'	=>'Choisissez le groupe de mots dont vous souhaitez fusionner des mots.', 
	'tous_groupes'		=>'Tous les groupes',
	'fusionmots'		=>'Fusionner des mots-clef',
	'vers'				=>'Vers',
	'remplacer'			=>'Fusionner la sélection',
	
);

?>